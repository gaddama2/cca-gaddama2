const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
const server = require('http').createServer(app);
const io = require('socket.io') (server);
app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
server.listen(port);

app.get("/", (req,res) => {
    res.sendFile(__dirname + '/static/chatclient.html');
})
io.on('connection', (socketclient) =>{
    console.log('A new client is connected!');
    socketclient.on("message", (data)=>{
        console.log('Data from a client: ' + data);
       // io.emit("message", `${socketclient.id} says: ${data}`); 
      BroadcastAuthenticatedClients("message",`${socketclient.id} says: ${data}`);
    });
    socketclient.on("typing", ()=>{
        console.log('Someone is typing....');
       // socketclient.broadcast.emit("typing", `${socketclient.id} is typing...`);
        BroadcastAuthenticatedClients("typing", `${socketclient.id||socketclient.id} is typing...`); 
    });
    socketclient.on("login",(username,password)=>{
     console.log(`Debug>Login data: ${username}/${password}`);
     login(username,password,(authenticated,message,account)=>{
        if (authenticated){
            socketclient.authenticated=true;
            socketclient.username=username;
            socketclient.account=account;
            socketclient.emit("authenticated");
            console.log(`Debug>${username} is authenticated`);
            var onlineClients = Object.keys(io.sockets.sockets).length; 
            var welcomemessage = `${socketclient.id} is connected! Number of connected clients: ${onlineClients}`
            console.log(welcomemessage);
            // io.emit("online", welcomemessage);    
            BroadcastAuthenticatedClients("online", welcomemessage);
        }else{
         socketclient.emit("login-failed",message);
         console.log(`Debug> Login failed:${username}/${password}`);
        }

       
     })
   
    });

    socketclient.on("signup",(fullname,username,email,password)=>{
     console.log(`Debug>Signup data: ${username}/${password}`);
     signup(fullname,username,email,password,(registered,message,account)=>{
        if (registered){
            socketclient.registered=true;
            
            socketclient.emit("registered");
            console.log(`Debug>${username} is registered`);
        }else{
         socketclient.emit("signup-failed");
         console.log(`Debug> signup failed:${username}/${password}`);
        }

       
     })
   
    });
});
io.on('connection', (socketclient) =>{
    //console.log('A new client is connected!');
    var onlineClients = Object.keys(io.sockets.sockets).length;
    var welcomemessage = `${socketclient.id} is connected! Number of connected clients: ${onlineClients}`
    console.log(welcomemessage);
   // io.emit("online", welcomemessage);    
    BroadcastAuthenticatedClients("online", welcomemessage); 

    socketclient.on("disconnect", ()=>{
    var onlineClients = Object.keys(io.sockets.sockets).length;
    var bymessage = `${socketclient.id} is disconnected! Number of connected clients: ${onlineClients}`
    console.log(bymessage);
    //io.emit("online", bymessage);  
    BroadcastAuthenticatedClients("online", bymessage); 
    });
});
const https = require ('https');
function login(username,password,callback){
    
    https.get('https://cca-gaddam-accountmicroservice.herokuapp.com/login/'+username+'/'+password,(resp)=>{
        let data = '';
        resp.on('data',(chunk)=>{
            data += chunk;

        });
        resp.on('end',()=>{
            console.log(data)
            const result = JSON.parse(data);
            console.log(result)
            console.log(result.status)
            if(!result || result.status==="found"){
               console.log("autheniticated")
               return callback(true,null,result.account)
           }else{
               callback(false,null,null)
           }
        })
       
    })
   
    

}
function signup(fullname,username,email,password,callback){
    https.get('https://cca-gaddam-accountmicroservice.herokuapp.com/signup/'+fullname+'/'+username+'/'+email+'/'+password,(resp)=>{
        let data = '';
        resp.on('data',(chunk)=>{
            data += chunk;
           console.log(data)
        });
        resp.on('end',()=>{
            if(data=="user already exists"){
                console.log("user already exists")
               return callback(false,null,null)
            }else if(data == "signup successful"){
                console.log("data")
                return callback(true,null,null)
            }
        })
    });
}
function BroadcastAuthenticatedClients(event,message){
    var sockets = io.sockets.sockets;
    for (var id in sockets){
        const socketclient = sockets[id];
        if (socketclient&&socketclient.authenticated){
            socketclient.emit(event,message);

        }
    }
}
