const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
const server = require('http').createServer(app);
const io = require('socket.io') (server);
app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
server.listen(port);

app.get("/", (req,res) => {
    res.sendFile(__dirname + '/static/chatclient.html');
})
io.on('connection', (socketclient) =>{
    console.log('A new client is connected!');
    socketclient.on("message", (data)=>{
        console.log('Data from a client: ' + data);
        io.emit("message", `${socketclient.id} says: ${data}`); 
    });
    socketclient.on("typing", ()=>{
        console.log('Someone is typing....');
        socketclient.broadcast.emit("typing", `${socketclient.id} is typing...`); 
    });
});
io.on('connection', (socketclient) =>{
    //console.log('A new client is connected!');
    var onlineClients = Object.keys(io.sockets.sockets).length;
    var welcomemessage = `${socketclient.id} is connected! Number of connected clients: ${onlineClients}`
    console.log(welcomemessage);
    io.emit("online", welcomemessage);    

    socketclient.on("disconnect", ()=>{
    var onlineClients = Object.keys(io.sockets.sockets).length;
    var bymessage = `${socketclient.id} is disconnected! Number of connected clients: ${onlineClients}`
    console.log(bymessage);
    io.emit("online", bymessage);  

    });
});

